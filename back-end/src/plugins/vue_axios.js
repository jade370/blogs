import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

// 如果有token, 就添加请求头, 作为用户, 否则没有, 视为游客 (未注册用户)
if (localStorage.getItem('token')) {
  axios.defaults.headers.common['Authorization'] =
    'Bearer ' + localStorage.getItem('token')
}

Vue.use(VueAxios, axios)
