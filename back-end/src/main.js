import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 导入element-ui安装文件
import '@/plugins/element_ui'
// 导入vue-particles插件安装文件
import '@/plugins/vue_particles'
// 导入vue_axios插件安装文件
// import '@/plugins/vue_axios'
// 导入富文本编辑器插件安装文件
import '@/plugins/vue_quill_editor'
import '@/plugins/http.js'
// 导入vue-echarts插件安装文件
import '@/plugins/echarts.js'

Vue.config.productionTip = false

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app')
console.dir(Vue)
