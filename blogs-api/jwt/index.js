//用一个单独模块来放生成token和验证token的方法，方便后面调用。
// const { secretKey } = require('./salt')

const jwt = require('jsonwebtoken')
const expressJwt = require('express-jwt')

//简易随机生成密钥
const secretKey = 'hello'

//生成 token, (要发送给客户端)
const createToken = (payload) =>
  // 参数1：用户的信息对象 如 {username: userinfo.username}
  // 参数2：加密的秘钥
  // 参数3：配置对象，可以配置当前 token 的有效期
  // 记住：千万不要把密码加密到 token 字符中
  jwt.sign(payload, secretKey, {
    expiresIn: 60 * 60 * 240, // 设置token的有效期 单位（秒）
  })

// JWT字符串解析: 验证 token
// 用于注册将 JWT 字符串解析还原成 JSON 对象的中间件
// 注意：只要配置成功了 express-jwt 这个中间件，就可以把解析出来的用户信息，挂载到 `req.user` 属性上
const jwtAuth = expressJwt({
  secret: secretKey,
  algorithms: ['HS256'],
  credentialsRequired: false,
  //  false：游客(未注册用户)(请求头中带有token) 可直接通行, 注册用户(请求头中带有token)仍校验
}).unless({
  path: [
    '/login/',
    '/login',
    // '/categorys/',
    // '/categorys',
    // '/articles/',
    // '/articles',
    // '/comments/',
    // '/comments',
    // '/messages/',
    // '/messages',
    '/upload/coverPicture/',
    '/upload/avatar/',
    '/download/coverPicture/',
    '/download/avatar/',
  ], //白名单: 不需要校验的路径
  // 结尾的"/"有严格要求, 请求"/:id" "/" 时要加, 请求"articles?id=1"时不加
})
module.exports = { jwtAuth, createToken }
