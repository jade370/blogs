var express = require('express')
var router = express.Router()
// 导入自己封装的数据库操作模块
const exec = require('../db')
//导入调试的标记
const { debug } = require('../config')

router.get('/', async function (req, res, next) {
  // 获取请求参数
  let query = req.query
  // 获取请求数据
  let body = req.body
  // 创建sql语句
  let sql

  /* 根据用户角色类型获取管理员信息 */
  if (query.role_name) {
    // GET /users/?role_name=admin
    sql = `select * from user_tb where role_name="${query.role_name}"`
  } else if (body.username && body.password && body.role_name == 'common') {
    /* 登录, 根据用户名和密码查询普通用户信息 */
    // GET users/
    sql = `select * from user_tb where username="${body.username}" and password="${body.password}" and role_name="${body.role_name}"`
  } else if (body.username && body.password && body.role_name == 'admin') {
    /* 根据用户名和密码查询用户表中的管理员信息 */
    // users/
    sql = `select * from user_tb where username="${body.username}" and password="${body.password}" and role_name="${body.role_name}"`
  } else if (query.page && query.size) {
    /* 分页查询分所有用户信息(不包含管理员) */
    // /users/?page=1&size=5
    let sql = `select count(*)-1 as total from user_tb`
    let { total } = (await exec(sql))[0] // { total: 9 }
    sql = `select * from user_tb where role_name='common' ORDER BY id desc limit ${
      query.size * (query.page - 1)
    },${query.size}`
    // 数据响应
    try {
      res.send({
        code: 0,
        msg: '查询成功',
        result: { total, users: await exec(sql) },
      })
      return
    } catch (err) {
      res.send({
        code: 1,
        msg: '失败',
        result: debug ? err : '',
      })
      return
    }
  } else {
    /* 获取所有用户信息 */
    // GET /users/
    sql = `select * from user_tb`
  }
  // 返回数据库操作结果
  try {
    res.send({
      code: 0,
      msg: '查询成功!',
      result: await exec(sql),
    })
  } catch (err) {
    res.send({
      code: 1,
      msg: '查询失败!',
      result: debug ? err : '',
    })
  }
})

/* 提交注册信息, 添加一个用户 */
// POST /users/
// 会给用户一个默认头像
router.post('/', async function (req, res, next) {
  // 获取请求数据
  let { username, password, email } = req.body
  // 创建sql语句
  let sql = `insert into user_tb (username,password,email, nickname, avatar) 
  values("${username}","${password}","${email}","${
    '用户' + Date.now()
  }", "http://localhost:3000/download/avatar/?img=default.png")`
  // 返回数据库操作结果
  try {
    let data = await exec(sql)
    res.send({
      code: 0,
      msg: '添加成功!',
      result: data.insertId,
    })
  } catch (err) {
    res.send({
      code: 1,
      msg: '添加失败!',
      result: debug ? err : '',
    })
  }
})

router.put('/', async function (req, res, next) {
  /* 根据id修改用户信息 */
  // PUT /users/
  // 获取请求参数
  let query = req.query
  // 获取请求数据
  let body = req.body
  // 过滤需要修改的数据
  let bodyfilter = ''
  for (let key in body) {
    if (body[key] !== '') {
      bodyfilter += `${key}="${body[key]}",`
    }
  }
  bodyfilter = bodyfilter.slice(0, bodyfilter.length - 1)
  // 创建sql语句
  let sql = `update user_tb set ${bodyfilter} where id=${query.id}`
  // 返回数据库操作结果
  try {
    let data = await exec(sql)
    res.send({
      code: 0,
      msg: '修改成功!',
      result: '',
    })
  } catch (err) {
    res.send({
      code: 1,
      msg: '修改失败!',
      result: debug ? err : '',
    })
  }
})

router.put('/:username', async function (req, res, next) {
  /* 根据username修改用户信息 */
  // PUT /users/
  // 获取请求参数
  let username = req.params.username
  // 获取请求数据
  let body = req.body
  // 过滤需要修改的数据
  let bodyfilter = ''
  for (let key in body) {
    if (body[key] !== '') {
      bodyfilter += `${key}="${body[key]}",`
    }
  }
  bodyfilter = bodyfilter.slice(0, bodyfilter.length - 1)
  // 创建sql语句
  let sql = `update user_tb set ${bodyfilter} where username="${username}"`
  // 返回数据库操作结果
  try {
    let data = await exec(sql)
    res.send({
      code: 0,
      msg: '修改成功!',
      result: '',
    })
  } catch (err) {
    res.send({
      code: 1,
      msg: '修改失败!',
      result: debug ? err : '',
    })
  }
})

router.delete('/', async function (req, res, next) {
  /* 根据id删除用户信息 */
  // DELETE /users/
  // 获取请求参数
  let query = req.query
  // 创建sql语句
  let sql = `delete from user_tb where id=${query.id}`
  // 返回数据库操作结果
  try {
    let data = await exec(sql)
    res.send({
      code: 0,
      msg: '删除成功!',
      result: '',
    })
  } catch (err) {
    res.send({
      code: 1,
      msg: '删除失败!',
      result: debug ? err : '',
    })
  }
})

// 导出路由对象
module.exports = router
