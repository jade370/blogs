module.exports = {
  // 标记是否是测试环境
  debug: true,

  mysql: {
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '1234',
    database: 'db_blogs',
    timezone: 'SYSTEM',
  },
}
