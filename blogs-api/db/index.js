//1. 导包
//导入mysql包
const mysql = require('mysql')
//导入数据库配置信息（dbConfig 取别名）
const { mysql: dbConfig } = require('../config')

//2. 创建连接，数据库参数可以放到配置文件中（config/index.js）
var con = mysql.createConnection(dbConfig)

//3. 连接数据库
con.connect()

// 封装一个数据库操作的方法(promise封装)
function exec(sql) {
  return new Promise((resolve, reject) => {
    con.query(sql, (err, data) => {
      if (err) reject(err) // 有错，把失败的数据带回去

      resolve(data) // 将获取到的数据带回去
    })
  })
}

//导出方法
module.exports = exec
