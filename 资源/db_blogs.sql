/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : db_blogs

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 12/10/2022 11:03:59
*/

-- 创建一个数据库
DROP DATABASE IF EXISTS `db_blogs`;
CREATE DATABASE db_blogs;

-- 使用数据库
USE db_blogs;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for article_tb
-- ----------------------------
DROP TABLE IF EXISTS `article_tb`;
CREATE TABLE `article_tb`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `title` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '文章标题',
  `cover_picture` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '文章封面路径',
  `description` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '文章描述',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '文章内容',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '文章创建时间',
  `edited_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '文章修改时间',
  `deleted_time` timestamp NULL DEFAULT NULL COMMENT '文章删除时间',
  `category_id` int NULL DEFAULT NULL COMMENT '文章类别',
  `visit_count` int NOT NULL DEFAULT 0 COMMENT '文章访问量',
  `is_recommend` tinyint NOT NULL DEFAULT 0 COMMENT '是否推荐, 推荐为1',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `category_id`(`category_id`) USING BTREE,
  CONSTRAINT `article_tb_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category_tb` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of article_tb
-- ----------------------------
INSERT INTO `article_tb` VALUES (1, '随笔1', 'http://localhost:3000/download/coverPicture/?img=default', '第一篇文章', '这是文章内容', '2022-03-05 10:12:39', '2022-08-17 21:56:27', NULL, 4, 0, 0);
INSERT INTO `article_tb` VALUES (2, '学习经验1', 'http://localhost:3000/download/coverPicture/?img=default', '第二篇文章', '这是文章内容', '2022-03-15 10:15:57', '2022-10-07 21:56:34', NULL, 5, 0, 1);
INSERT INTO `article_tb` VALUES (3, '前端笔记1', 'http://localhost:3000/download/coverPicture/?img=default', '第三篇文章', '这是文章内容', '2022-05-20 10:16:32', '2022-08-26 21:52:25', NULL, 1, 0, 0);
INSERT INTO `article_tb` VALUES (4, '后端笔记', 'http://localhost:3000/download/coverPicture/?img=default', '第四篇文章', '这是文章内容', '2022-06-18 10:17:20', '2022-09-14 21:52:34', NULL, 2, 0, 1);
INSERT INTO `article_tb` VALUES (5, 'vue1', 'http://localhost:3000/download/coverPicture/?img=default', '第五篇文章', '这是文章内容', '2022-07-07 10:17:50', '2022-10-01 21:52:42', NULL, 3, 0, 0);
INSERT INTO `article_tb` VALUES (6, 'windows技巧1', 'http://localhost:3000/download/coverPicture/?img=default', '第六篇文章', '这是文章内容', '2022-10-09 10:18:22', '2022-10-07 21:52:50', NULL, 6, 0, 1);

-- ----------------------------
-- Table structure for category_tb
-- ----------------------------
DROP TABLE IF EXISTS `category_tb`;
CREATE TABLE `category_tb`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `category_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '文章类别名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of category_tb
-- ----------------------------
INSERT INTO `category_tb` VALUES (1, '前端笔记');
INSERT INTO `category_tb` VALUES (2, '后端笔记');
INSERT INTO `category_tb` VALUES (3, 'vue');
INSERT INTO `category_tb` VALUES (4, '随笔');
INSERT INTO `category_tb` VALUES (5, '学习经验');
INSERT INTO `category_tb` VALUES (6, 'windows技巧');

-- ----------------------------
-- Table structure for comment_tb
-- ----------------------------
DROP TABLE IF EXISTS `comment_tb`;
CREATE TABLE `comment_tb`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评论内容',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '评论时间',
  `user_id` int NOT NULL COMMENT '用户id_外键',
  `article_id` int NOT NULL COMMENT '文章id_外键',
  `approved` tinyint NOT NULL DEFAULT '1' COMMENT '是否通过审核',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `article_id`(`article_id`) USING BTREE,
  CONSTRAINT `comment_tb_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_tb` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comment_tb_ibfk_2` FOREIGN KEY (`article_id`) REFERENCES `article_tb` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of comment_tb
-- ----------------------------
INSERT INTO `comment_tb` VALUES (1, '评论111', '2022-10-07 09:39:09', 1, 1, 1);
INSERT INTO `comment_tb` VALUES (2, '评论222', '2022-10-07 09:39:40', 1, 2, 1);
INSERT INTO `comment_tb` VALUES (3, '评论333', '2022-10-07 09:39:40', 1, 2, 1);
INSERT INTO `comment_tb` VALUES (4, '评论444', '2022-10-07 09:39:40', 1, 2, 1);

-- ----------------------------
-- Table structure for message_tb
-- ----------------------------
DROP TABLE IF EXISTS `message_tb`;
CREATE TABLE `message_tb`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '留言id',
  `content` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '留言内容',
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of message_tb
-- ----------------------------
INSERT INTO `message_tb` VALUES (1, '留言111', '2022-10-15 15:29:07');
INSERT INTO `message_tb` VALUES (2, '留言222', '2022-10-15 15:29:07');
INSERT INTO `message_tb` VALUES (3, '留言333', '2022-10-15 15:29:07');
INSERT INTO `message_tb` VALUES (4, '留言444', '2022-10-15 15:29:07');

-- ----------------------------
-- Table structure for user_tb
-- ----------------------------
DROP TABLE IF EXISTS `user_tb`;
CREATE TABLE `user_tb`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `role_name` enum('admin','common') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'common' COMMENT '用户角色',
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户邮箱',
  `wechat` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '用户微信',
  `avatar` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '用户头像路径',
  `nickname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '默认昵称' COMMENT '用户昵称',
  `signature` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_tb
-- ----------------------------
INSERT INTO `user_tb` VALUES (1, 'admin', 'admin', '1234', '12345678@qq.com', 'vx112233', 'http://localhost:3000/download/avatar/?img=default', '喵~', '只要你说一声，天涯海角，我都会回来');
INSERT INTO `user_tb` VALUES (2, 'common', 'user1', '111', '10210236@qq.com', NULL, 'http://localhost:3000/download/avatar/?img=default.png', '秦~', NULL);
INSERT INTO `user_tb` VALUES (3, 'common', 'user2', '111', '10210236@qq.com', NULL, 'http://localhost:3000/download/avatar/?img=default.png', '康~', NULL);
INSERT INTO `user_tb` VALUES (4, 'common', 'user3', '111', '10210234@qq.com', NULL, 'http://localhost:3000/download/avatar/?img=default.png', '王~', NULL);
INSERT INTO `user_tb` VALUES (5, 'common', 'user4', '111', '12342345@qq.com', NULL, 'http://localhost:3000/download/avatar/?img=default.png', '陈~', NULL);

SET FOREIGN_KEY_CHECKS = 1;
