import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 导入element-ui安装文件
import './plugins/element.js'
// 导入vue-axios插件安装文件
// import './plugins/vue_axios.js'
import './plugins/http.js'
// 导入富文本编辑器插件安装文件
import './plugins/vue_quill_editor'
// 导入弹幕插件安装文件
// import './plugins/vue-danmaku'

Vue.config.productionTip = false

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app')
