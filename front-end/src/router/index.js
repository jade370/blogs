import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  //设置一级路由

  {
    path: '/',
    redirect: '/home', //重定向
  },
  {
    path: '/',
    name: 'home',
    component: () => import('../views/HomeView'),
    //设置二级路由
    children: [
      {
        path: 'home',
        component: () => import('../views/HomeView/HomeViewHome'),
      },
      {
        path: 'article',
        component: () => import('../views/HomeView/HomeViewArticle'),
      },
      {
        path: 'category',
        component: () => import('../views/HomeView/HomeViewCategory'),
      },
      {
        path: 'file',
        component: () => import('../views/HomeView/HomeViewFile'),
      },
      {
        path: 'message',
        component: () => import('../views/HomeView/HomeViewMessage'),
      },
      {
        path: 'friend',
        component: () => import('../views/HomeView/HomeViewFriends'),
      },
      {
        path: 'brief',
        component: () => import('../views/HomeView/HomeViewBrief'),
      },
    ],
  },
  {
    path: '*',
    name: 'notfound',
    component: () => import('../views/NotFound.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

// 路由守卫

export default router
